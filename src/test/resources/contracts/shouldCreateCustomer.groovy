package contracts

import org.springframework.cloud.contract.spec.Contract
import org.springframework.http.HttpHeaders

import static org.springframework.http.HttpHeaders.CONTENT_TYPE
import static org.springframework.http.HttpHeaders.CONTENT_TYPE

Contract.make {
    description 'should create customer'
    request {
        url '/api/customers'
        method POST()
        body([firstName: 'Marek',
              lastName: 'Kapowicki',
              'email' : 'marek.kapowicki@gmail.com'])
        headers {
            header CONTENT_TYPE, 'application/vnd.microlending.v1+json'
        }
    }
    response {
        status 201
        headers {
            header(CONTENT_TYPE, 'application/vnd.microlending.v1+json;charset=UTF-8')
        }
        body(['id' : '123'])

    }

}