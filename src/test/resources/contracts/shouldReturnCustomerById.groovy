package contracts

import org.springframework.cloud.contract.spec.Contract
import org.springframework.http.HttpHeaders

import static org.springframework.http.HttpHeaders.CONTENT_TYPE

Contract.make {
    description 'should return marek'
    request {
        url '/api/customers/1'
        method GET()
    }
    response {
        status 200
        headers {
            header(CONTENT_TYPE, 'application/vnd.microlending.v1+json' + ';charset=UTF-8')
        }
        body([identityNo: '1', fullName: 'Marek Kapowicki',    'email' : 'marek.kapowicki@gmail.com'])
    }

}