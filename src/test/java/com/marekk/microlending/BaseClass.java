package com.marekk.microlending;

import com.jayway.restassured.module.mockmvc.RestAssuredMockMvc;
import com.marekk.microlending.domain.customer.CustomerFacade;
import com.marekk.microlending.domain.customer.Customers;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.context.WebApplicationContext;

import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = MicrolendingApplication.class)
public class BaseClass {
    @Autowired
    private WebApplicationContext context;

    @MockBean
    private CustomerFacade customerFacade;

    @Before
    public void setUp() throws Exception {
        given(customerFacade.retrieve("1")).willReturn(Customers.MAREK);
        given(customerFacade.register(any())).willReturn("123");
        RestAssuredMockMvc.webAppContextSetup(this.context);
    }
}
